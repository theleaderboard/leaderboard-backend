const mongoose = require("mongoose");

const { Schema } = mongoose;

// create schema
const UserSchema = new Schema({
  name: {
    type: String,
    required: true,
  },
  username: {
    type: String,
    unique: true,
    required: true,
    minlength: 8,
    maxlength: 50,
    validate: {
      validator(name) {
        const regex = /^[a-zA-Z0-9_]+$/;
        return regex.test(name);
      },
      message: "Username must consist only of letters, numbers, and underscores.",
    },
  },
  email: {
    type: String,
    required: true,
    unique: true,
    lowercase: true,
    trim: true,
  },
  password: {
    type: String,
    required: true,
  },
  created_date: {
    type: Date,
    default: Date.now,
  },
});

UserSchema.virtual("managed_leagues", {
  ref: "League",
  localField: "_id",
  foreignField: "commissioner",
});

const User = mongoose.model("users", UserSchema);

module.exports = User;
